#!/usr/bin/python
from distutils.core import setup
from distutils.extension import Extension
import commands

def pkgconfig(*packages, **kw):
    flag_map = {'-I': 'include_dirs', '-L': 'library_dirs', '-l': 'libraries'}
    for token in commands.getoutput("pkg-config --libs --cflags %s" % ' '.join(packages)).split():
        if flag_map.has_key(token[:2]):
            kw.setdefault(flag_map.get(token[:2]), []).append(token[2:])
        else:
            kw.setdefault('extra_link_args', []).append(token)
    return kw

pygmyth = Extension('pygmyth',
                    sources = ['pygmyth.c'],
                    **pkgconfig('gobject-2.0', 'gmyth',
                                include_dirs=["/usr/local/include/gmyth"])
                   )

setup (name = 'pygmyth',
       version = '1.0',
       description = 'Python package for accessing MythTV',
       ext_modules = [pygmyth])
