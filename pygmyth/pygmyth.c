#include <Python.h>
#include <stdio.h>
#include <glib.h>

#include "gmyth_backendinfo.h"
#include "gmyth_file_transfer.h"
#include "gmyth_scheduler.h"
#include "gmyth_util.h"
#include "gmyth_epg.h"

typedef struct
{
    PyObject_HEAD
    GMythBackendInfo *options;
} Pygmyth;

static void pygmyth_dealloc(Pygmyth *self)
{
    if(self->options)
        g_object_unref(self->options);
    self->ob_type->tp_free((PyObject*)self);
}

void _set_connection_parameters(char    *host_ip,
                                int     host_port,
                                char    *username,
                                char    *password,
                                GMythBackendInfo *options)
{
    gmyth_backend_info_set_hostname(options, host_ip);
    gmyth_backend_info_set_port(options, host_port);

    if (username)
        gmyth_backend_info_set_username(options, username);
    else
        gmyth_backend_info_set_username(options, "mythtv");

    if (password)
        gmyth_backend_info_set_password(options, password);
    else
        gmyth_backend_info_set_password(options, "mythtv");

    gmyth_backend_info_set_db_name(options, "mythconverg");
}

    
static PyObject *pygmyth_file(Pygmyth *self, PyObject *args)
{
    GMythBackendInfo *options = self->options;
    PyObject *result;
    PyThreadState *_save;
    const char *filename;
    GMythFileTransfer *transfer;
    gint            file_transf_ret;
    GArray         *array = NULL;

    if(!PyArg_ParseTuple(args, "s", &filename))
        return NULL;

    _save = PyEval_SaveThread();

    if(!gmyth_util_file_exists(options, filename)) {
        PyEval_RestoreThread(_save);
        PyErr_SetString(PyExc_IOError, "File not found on the server.");
        return NULL;
    }

    transfer = gmyth_file_transfer_new(options);
    if (!gmyth_file_transfer_open(transfer, filename)) {
        PyEval_RestoreThread(_save);
        PyErr_SetString(PyExc_IOError, "File could not be opened.");
        return NULL;
    }

    array = g_array_new(TRUE, TRUE, sizeof(gchar));

    file_transf_ret = gmyth_file_transfer_read (GMYTH_FILE_TRANSFER(transfer),
	                                           (GByteArray *) array, 64000, FALSE);

    while ((file_transf_ret == GMYTH_FILE_READ_OK) ||
           (file_transf_ret == GMYTH_FILE_READ_NEXT_PROG_CHAIN)) {
        
        file_transf_ret = gmyth_file_transfer_read (GMYTH_FILE_TRANSFER(transfer),
                                                 (GByteArray *) array, 64000, FALSE);
    }

    gmyth_file_transfer_close(transfer);

    PyEval_RestoreThread(_save);

    if(array->len > 0)
        result = Py_BuildValue("s#", array->data, array->len-1);
    else
    {
        result = Py_None;
        Py_INCREF(Py_None);
    }

    return result;
}

static PyObject *pygmyth_channels(Pygmyth *self)
{
    GMythBackendInfo *options = self->options;
    GMythEPG       *epg;
    gint            length;
    GList          *clist,
                   *ch;
    PyObject *result, *item;
    PyThreadState *_save;

    _save = PyEval_SaveThread();

    epg = gmyth_epg_new();
    if (!gmyth_epg_connect(epg, options)) {
        PyEval_RestoreThread(_save);
        PyErr_SetString(PyExc_IOError, "Could not connect to backend db");
        g_object_unref(epg);
        return NULL;
    }

    length = gmyth_epg_get_channel_list(epg, &clist);

    PyEval_RestoreThread(_save);
    
    result = Py_BuildValue("[]");

    for (ch = clist; ch != NULL; ch = ch->next) {
        GMythChannelInfo *info = (GMythChannelInfo *) ch->data;

        if ((info->channel_name == NULL) || (info->channel_num == NULL)) {
            continue;
        }
        item = Py_BuildValue("sss",  info->channel_num->str,
                                     info->channel_name->str,
                                     info->channel_icon->str);

        PyList_Append(result, item);
        Py_DECREF(item);
    }

    gmyth_free_channel_list(clist);
    gmyth_epg_disconnect(epg);
    g_object_unref(epg);

    return result;
}

static PyObject *pygmyth_recordings(Pygmyth *self)
{
    GMythBackendInfo *options = self->options;
    GMythScheduler *scheduler;
    GList          *list = NULL, *iter = NULL;
    gint            res = 0;
    PyObject *result, *item;
    PyThreadState *_save;

    _save = PyEval_SaveThread();

    scheduler = gmyth_scheduler_new();

    if (gmyth_scheduler_connect_with_timeout(scheduler,
                                             options, 10) == FALSE)
    {
        PyEval_RestoreThread(_save);
        PyErr_SetString(PyExc_IOError, "Could not connect to MythTV Scheduler");
        g_object_unref(scheduler);
        return NULL;
    }

    res = gmyth_scheduler_get_recorded_list(scheduler, &list);
    gmyth_scheduler_disconnect(scheduler);
    g_object_unref(scheduler);
    
    if (res <= 0)
    {
        PyEval_RestoreThread(_save);
        PyErr_SetString(PyExc_IOError, "No files were found in the backend.");
        return NULL;
    }

    iter = list;

    while (iter) {
        RecordedInfo   *recorded_info = (RecordedInfo *) iter->data;

        if (gmyth_util_file_exists
            (options, recorded_info->basename->str))
        {
            iter = g_list_next(iter);
        }
        else
        {
            gmyth_recorded_info_free(recorded_info);

            if (iter->prev == NULL)
                /* Start of the list */
                list = iter->next;

            iter = g_list_delete_link(iter, iter);
        }
    }


    PyEval_RestoreThread(_save);

    iter = list;
    result = Py_BuildValue("[]");

    while (iter) {
        RecordedInfo   *recorded_info = (RecordedInfo *) iter->data;

        float starttime, endtime;

        starttime = (float)recorded_info->start_time->tv_sec +
                    ((float)recorded_info->start_time->tv_usec) / 1000000;

        endtime = (float)recorded_info->end_time->tv_sec +
                    ((float)recorded_info->end_time->tv_usec) / 1000000;

        item = Py_BuildValue("ssssff", recorded_info->basename->str,
                                        recorded_info->title->str,
                                        recorded_info->subtitle->str,
                                        recorded_info->description->str,
                                        starttime, endtime);
        PyList_Append(result, item);
        Py_DECREF(item);

        gmyth_recorded_info_free(recorded_info);
        iter = g_list_next(iter);
    }

    g_list_free(list);

    return result;
}

static PyObject *
pygmyth_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    Pygmyth *self;

    self = (Pygmyth *)type->tp_alloc(type, 0);

    return (PyObject *)self;
}

static int
pygmyth_init(Pygmyth *self, PyObject *args, PyObject *kwds)
{
    char *host_ip = NULL, *username = NULL, *password = NULL;
    int host_port = 6543;

    static char *kwlist[] = {"host_ip",
                             "host_port",
                             "username",
                             "password",
                             NULL};

    if(! PyArg_ParseTupleAndKeywords(args, kwds, "siss", kwlist,
                                     &host_ip, &host_port,
                                     &username, &password))
            return -1;


    GMythBackendInfo *options = g_new0(GMythBackendInfo, 1);
    options = gmyth_backend_info_new();

    _set_connection_parameters(host_ip, host_port, username, password, options);

    self->options = options;

    return 0;
}

static PyMethodDef pygmyth_methods[] = {
    {"file", (PyCFunction)pygmyth_file, METH_VARARGS,
    "Return a string containing file data"},
    {"channels", (PyCFunction)pygmyth_channels, METH_NOARGS,
    "Return a list of available channels"},
    {"recordings", (PyCFunction)pygmyth_recordings, METH_NOARGS,
     "Return a list of available recordings"},
    {NULL}  /* Sentinel */
};

static PyTypeObject pygmyth_pygmythType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "pygmyth.pygmyth",         /*tp_name*/
    sizeof(Pygmyth),           /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor)pygmyth_dealloc,   /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,     /*tp_flags*/
    "pygmyth objects",           /* tp_doc */
    0,		                   /* tp_traverse */
    0,		                   /* tp_clear */
    0,		                   /* tp_richcompare */
    0,		                   /* tp_weaklistoffset */
    0,		                   /* tp_iter */
    0,		                   /* tp_iternext */
    pygmyth_methods,           /* tp_methods */
    0,           /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)pygmyth_init,      /* tp_init */
    0,                         /* tp_alloc */
    pygmyth_new,                 /* tp_new */
};


static PyMethodDef module_methods[] = {
    {NULL}  /* Sentinel */
};

#ifndef PyMODINIT_FUNC	/* declarations for DLL import/export */
#define PyMODINIT_FUNC void
#endif
PyMODINIT_FUNC
initpygmyth(void)
{
    PyObject *m;

    g_type_init();
    if (!g_thread_supported ()) g_thread_init (NULL);
    
    if (PyType_Ready(&pygmyth_pygmythType) < 0)
        return;

    m = Py_InitModule3("pygmyth", module_methods,
                       "Interface for working with mythtv.");

    if (m == NULL)
      return;

    Py_INCREF(&pygmyth_pygmythType);
    PyModule_AddObject(m, "pygmyth", (PyObject *)&pygmyth_pygmythType);
}
