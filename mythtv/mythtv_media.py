# -*- coding: utf-8 -*-
# Elisa - Home multimedia server
# Copyright (C) 2006-2008 Fluendo Embedded S.L. (www.fluendo.com).
# All rights reserved.
#
# This file is available under one of two license agreements.
#
# This file is licensed under the GPL version 3.
# See "LICENSE.GPL" in the root of this distribution including a special
# exception to use Elisa with Fluendo's plugins.
#
# The GPL part of Elisa is also available under a commercial licensing
# agreement from Fluendo.
# See "LICENSE.Elisa" in the root directory of this distribution package
# for details on that license.

__maintainer__ = 'Stephen English <steve@secomputing.co.uk>'

from elisa.base_components.media_provider import MediaProvider, UriNotMonitorable
from elisa.base_components.media_provider import NotifyEvent
from elisa.core.component import InitializeFailure
from elisa.core.media_uri import MediaUri
from elisa.core.media_file import MediaFile
from elisa.core import common
from elisa.core.observers.dict import DictObservable
from twisted.internet import defer, threads

from elisa.extern.translation import gettexter, N_
#T_ = gettexter('elisa-mythtv')

import os, gst
import pygmyth

from datetime import datetime

plugin_registry = common.application.plugin_registry
NetworkLocationMessage = \
plugin_registry.get_component_class('base:local_network_location_message')

class SizedDict(dict):
    ''' Sized dictionary without timeout. '''

    def __init__(self, size=1000):
        dict.__init__(self)
        self._maxsize = size
        self._stack = []

    def __setitem__(self, name, value):
        if len(self._stack) >= self._maxsize:
            self.__delitem__(self._stack[0])
            del self._stack[0]
        self._stack.append(name)
        return dict.__setitem__(self, name, value)


class MythtvMedia(MediaProvider):
    """
    This class implements local filesystem support
    """
    default_config = {'host_ip': '',
                      'host_port' : 6543,
                      'username' : 'mythtv',
                      'password' : 'mythtv'}

    config_doc = {'host_ip' : '',
                  'host_port' : '',
                  'username' : '',
                  'password' : ''}
        
    def initialize(self):
        try:
            gst.element_factory_make('mythtvsrc')
        except gst.PluginNotFoundError:
            msg = "Can't initialize gstreamer mythtvsrc plugin"
            raise InitializeFailure(self.name, msg)

        self.host_ip = self.config.get('host_ip', '')
        self.host_port = self.config.get('host_port', 6543)

        self.thumbcache = SizedDict(size=100) # How many thumbnails to cache
        self.recordings = None

        try:
            self._client = pygmyth.pygmyth(self.host_ip,
                                           self.host_port,
                                           self.config.get('username', 'mythtv'),
                                           self.config.get('password', 'mythtv'))
        except:
            self._client = None

        uri = "mythtv:///"

        action_type = NetworkLocationMessage.ActionType.LOCATION_ADDED
        msg = NetworkLocationMessage(action_type, "MythTV", 'mythtv', uri,
                                     media_types=['video'],
                                     theme_icon='mythtv')
        common.application.bus.send_message(msg)
        
    def scannable_uri_schemes__get(self):
        return {}
    
    def supported_uri_schemes__get(self):
        return { 'mythtv': 0 }

    def get_media_type(self, uri):
        dfr = defer.Deferred()
        if self._is_directory(uri):
            dfr.callback({'file_type': 'directory', 'mime_type': ''})
        else:
            if uri.host == 'video' or uri.host == 'channel':
                dfr.callback({'file_type': 'video', 'mime_type': 'video/x-mpeg'})
            elif uri.host == 'thumbnail':
                dfr.callback({'file_type': 'image', 'mime_type': 'image/png'})

        return dfr

    def get_real_uri(self, uri):
        real_uri = None
        if uri.host == 'video':
            video_id = uri.path[1:]
            video_url = MediaUri("myth://%s:%d/%s" % (self.host_ip,
                                                    self.host_port,
                                                    video_id))
        elif uri.host == 'channel':
            channel_id = uri.path[1:]
            video_url = MediaUri("myth://%s:%d/?channel=%s" % (self.host_ip,
                                                               self.host_port,
                                                               channel_id))
        elif uri.host == 'thumbnail':
            video_url = MediaUri(uri)
        else:
            video_url = None
        
        return video_url

    def has_children_with_types(self, uri, media_types):
        dfr = defer.Deferred()
        
        has_children = False
        if self._is_directory(uri):
            if 'video' in media_types:
                has_children = True

        dfr.callback(has_children)
        return dfr

    def _is_directory(self, uri):
        return uri.scheme == 'mythtv' and not (uri.host in ('video',
                                                            'channel',
                                                            'thumbnail'))

    def _build_main_menu(self, children):
        self.debug("Building main menu")
        child_uri = MediaUri('mythtv://recordings')
        child_uri.label = "Recordings" #T_(N_("Recordings"))
        children.append((child_uri, {}))

        child_uri = MediaUri('mythtv://channels')
        child_uri.label = "Channels"
        children.append((child_uri, {}))

        return children

    def _retrieve_children(self, uri, children):
        """
        Run in a defertothread
        """

        host = uri.host
        path = uri.path

        if host == '':
            children = self._build_main_menu(children)
            self.recordings = self._client.recordings()
        elif host == 'recordings':

            if self.recordings == None:
                self.recordings = self._client.recordings()

            if path == '':
                child_uri = MediaUri('mythtv://recordings/all')
                child_uri.label = "All Recordings" #T_(N_("Recordings"))
                children.append((child_uri, {}))

                series = list(set([x[1] for x in self.recordings]))
                series.sort()

                def sortrecbydate(a, b):
                    if a[4] > b[4]:
                        return 1
                    if a[4] < b[4]:
                        return -1
                    return 0
                    
                self.recordings.sort(cmp=sortrecbydate)

                for s in series:
                    #Get the most recent recording
                    prog = [x for x in self.recordings if x[1] == s][-1]

                    metadata = DictObservable()
                    metadata['default_image'] = \
                        MediaUri("mythtv://thumbnail/%s.png" % prog[0])

                    child_uri = MediaUri('mythtv://recordings/%s' % s)
                    child_uri.label = s #T_(N_("Recordings"))
                    children.append((child_uri, metadata))

            else:
                if path[1:] != "all":
                    r = [x for x in self.recordings if x[1] == path[1:]]
                else:
                    r = self.recordings

                def sortrec(a, b):
                    if a[1] > b[1]:
                        return 1
                    if a[1] < b[1]:
                        return -1
                    return 0

                r.sort(cmp=sortrec)
                for rec in r:
                    child_uri = MediaUri('mythtv://video/%s' % rec[0])
                    child_uri.label = rec[1]
                    #child_uri.label = "%s - %s" % (rec[1],
                    #    datetime.utcfromtimestamp(rec[4]).strftime( \
                    #                              "%a, %d %b %Y %H:%M:%S"))

                    metadata = DictObservable()
                    metadata['default_image'] = \
                        MediaUri("mythtv://thumbnail/%s.png" % rec[0])

                    children.append((child_uri, metadata))
        
        elif host == 'channels':
            channels = self._client.channels()
            
            def sortchan(a, b):
                if a[0] > b[0]:
                    return 1
                if a[0] < b[0]:
                    return -1
                return 0
            
            channels.sort(cmp=sortchan)

            for channel in channels:
                child_uri = MediaUri('mythtv://channel/%s' % channel[1])
                child_uri.label = "%s - %s" % (channel[0], channel[1])
                
                metadata = DictObservable()
                if channel[2] == '':
                    metadata['default_image'] = MediaUri("file://%s/mythtv.png"\
                                                        % self.plugin.directory)
                else:
                    metadata['default_image'] = MediaUri("file://%s" % \
                                                         channel[2])

                children.append((child_uri, metadata))

        return children


    def get_direct_children(self, uri, l):
        d = threads.deferToThread(self._retrieve_children, uri, l)
        return d

    def _blocking_open(self, uri, mode=None):
        if uri.host == 'thumbnail':
            file = uri.path[1:]
            if not file in self.thumbcache:
                self.thumbcache[file] = {"File" : self._client.file(file),
                                         "Pos"  : 0}

            return MediaFile(self, file)

    def open(self, uri, mode='r'):
        d = threads.deferToThread(self._blocking_open, uri, mode)
        return d

    def read(self, media_file, size=-1):
        d = threads.deferToThread(self._blocking_read, media_file, size)
        return d

    def _blocking_read(self, media_file, size=-1):
        uri = media_file.descriptor
        try:
            data = self.thumbcache[uri]
        except KeyError:
            return None

        if size == -1:
            data["Pos"] = 0
            return data["Data"]
        else:
            if size < len(data["Data"])-data["Pos"]:
                d = data["Data"][data["Pos"]:data["Pos"]+size]
                data["Pos"] = data["Pos"] + size
                return d

    def uri_is_monitorable(self, uri):
        return False
